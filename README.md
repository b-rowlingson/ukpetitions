# Make a map from a petition

## Create a petition object

```
> pd = petition(electoral_map_geo(), petition_pop(), petition_const())
```

## Add petition data to it

```
> pd = add_petition(pd, 241584)
```

## Convert to spatial and plot

```
> sm = st_petition(pd, 241584)
> plot(sm["signature_count"])
```

## Coming soon

 * Population counts and ratios.
 
## Credits

Contains map data derived from GeoJSON and TopoJSON UK Boundary Data
by Martin Chorley, licensed under a Creative Commons Attribution 4.0
International License.
